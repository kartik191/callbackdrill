const fs = require('fs');


function createDirectory(dir, callback){
    fs.mkdir(dir, err => {
        if(err){
            console.error(err);
        }
        else{
            callback();
        }
    });
}

function writeFile(path, data, callback){
    fs.writeFile(path, data, err => {
        if(err){
            console.error(err);
        }
        else{
            callback();
        }
    });
}

function removeFiles(dir){
    fs.readdir(dir, (err, data) => {
        if(err){
            console.error(err);
        }
        else{
            data.forEach(file => {
                fs.unlink(dir + file, (err) => {
                    if(err){
                        console.error(err);
                    }
                });
            });
        }
    });
}

function problem1(){
    let data = 'random text';
    const dir = __dirname + '/json/'; 
    createDirectory(dir, () => {
        writeFile(dir + 'file1.json', data, () => {
            writeFile(dir + 'file2.json', data, () => {
                writeFile(dir + 'file3.json', data, () => {
                    removeFiles(dir);
                });
            });
        });
    });   
}


module.exports = problem1;