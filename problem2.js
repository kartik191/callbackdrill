const fs = require('fs');


function readFile(path, callback){
    fs.readFile(path, 'utf-8', (err, data) => {
        if(err){
            console.error(err);
        }
        else{
            callback(data);
        }
    });
}

function writeFile(path, data, callback){
    fs.writeFile(path, data, err => {
        if(err){
            console.error(err);
        }
        else{
            callback();
        }
    });
}

function appendFile(path, data, callback){
    fs.appendFile(path, data, err => {
        if(err){
            console.error(err);
        }
        else {
            callback();
        }
    });
}
 
function removeFiles(dir, filenamesPath){
    readFile(filenamesPath, data => {
        data.split('\n').forEach(file => {
            if(file !== ''){
                fs.unlink(dir + file, (err) => {
                    if(err){
                        console.error(err);
                    }
                });
            }
        });                        
    });
}

function problem2(){
    const dir = __dirname + '/';
    const lipsumPath = dir + 'lipsum.txt';
    const filenamesPath = dir + 'filenames.txt';

    readFile(lipsumPath, data => {
        const uppercasePath = dir + 'uppercase.txt';
        writeFile(uppercasePath, data.toUpperCase(), () => {
            appendFile(filenamesPath, 'uppercase.txt\n', () => {
                readFile(uppercasePath, data => {
                    data = data.toLowerCase().replaceAll('. ','.\n');
                    const lowercasePath = dir + 'lowercase.txt';
                    writeFile(lowercasePath, data, () => {
                        appendFile(filenamesPath, 'lowercase.txt\n', () => {
                            readFile(lowercasePath, data => {
                                data = data.split('\n').sort().join('\n'); 
                                const sortPath = dir + 'sort.txt'; 
                                writeFile(sortPath, data, () => {
                                    appendFile(filenamesPath, 'sort.txt\n', () => {
                                        removeFiles(dir, filenamesPath);
                                    });
                                });
                            });
                        });
                    });
                });
            });
        });
    });
}


module.exports = problem2;